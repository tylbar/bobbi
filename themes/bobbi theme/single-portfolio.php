<?php
/**
 *  Theme:
 *  File: single-portfolio.php
 *  Author: Tyler Barnes
 */

get_header();?>

<main>
  <?php
  $field = get_field_object('layout_style');
  ?>

  <?php
  if( $field )
  {
          foreach( $field['choices'] as $v )
          {
              if (get_field('layout_style') == $v):

                get_template_part('portfolio-layout/'.$v);

              endif;
          }
  } ?>
</main>

<?php
get_footer();
?>
