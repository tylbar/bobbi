var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var jade = require('gulp-jade-php');

gulp.task('serve', ['sass', 'jadephp'], function() {

  browserSync.init({
    proxy: 'bobbi.dev/'
  });

  gulp.watch('scss/*.scss', ['sass']);
  gulp.watch('./jadephp/**/*.jade', ['jadephp']);
  gulp.watch('*.php').on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("scss/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("./"))
        .pipe(browserSync.stream());
});

// Compile jade to php
gulp.task('jadephp', function() {
  gulp.src('./jadephp/**/*.jade')
    .pipe(jade({
        locals: {
          title: 'title'
        }
     }))
     .pipe(gulp.dest('./'))
     .pipe(browserSync.stream());
});



gulp.task('default', ['serve']);
