<?php
/**
 *  Theme:
 *  File: front-page.php
 *  Author: Tyler Barnes
 */

get_header(); ?>

<main>

	<section class="main-left">
		<img class='logo' src="<?php echo get_stylesheet_directory_uri(); ?>/img/portfolio logo.svg" alt="Portfolio logo">

		<ul class='contact-info-text'>
			<?php
				echo '<h1>';

				if (get_field('first_name', 'option')):
					echo the_field('first_name', 'option'); echo '<br><hr>';
				endif;

				if (get_field('last_name', 'option')):
					echo the_field('last_name', 'option');
				endif;

				echo '</h1>';

				if (have_rows('contact_information', 'option')):
					while (have_rows('contact_information', 'option')) : the_row();
						echo '<li>'; the_sub_field('line'); echo '</li>';
					endwhile;
				endif;
			?>
		</ul>

		<nav>
				<?php wp_nav_menu(); ?>
		</nav>

	</section>
	<section class="main-right">
		<h2><?php if(get_field('right_half_top_left_text')): echo the_field('right_half_top_left_text'); endif; ?></h2>
		<h2><?php if(get_field('right_half_top_right_text')): echo the_field('right_half_top_right_text'); endif; ?></h2>
		<p><?php if(get_field('right_half_bottom_text')): echo the_field('right_half_bottom_text'); endif; ?></p>
	</section>
</main>

<?php get_footer(); ?>
