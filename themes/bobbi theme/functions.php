<?php
/**
 *  Theme:
 *  File: functions.php
 *  Author: Tyler Barnes
 */

add_action( 'wp_enqueue_scripts', 'load_jquery' );
function load_jquery() {
    wp_enqueue_style( 'theme-css', get_stylesheet_uri());

    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', get_bloginfo('url').'/wp-includes/js/jquery/jquery.js', false,null, true );
    wp_enqueue_script( 'jquery' );

    // wp_enqueue_script( 'all', get_template_directory_uri() . '/js/all.js', array('jquery'), null, true );
}

if(function_exists('register_nav_menus')):
    register_nav_menus(array(
        'main' => 'Main Navigation'
    ));
endif;

add_theme_support('post-thumbnails');

// hide WP meta from hackers
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');

// add custom variables
add_action('wp_head','site_url_script');
function site_url_script() {
 ?>
 <script type="text/javascript">
  var ajaxUrl = '<?php echo admin_url('admin-ajax.php'); ?>';
  var siteUrl = '<?php echo get_bloginfo('url'); ?>';
 </script>
<?php
}

function debug($var) {
    $bt = debug_backtrace();
    $caller = array_shift($bt);
    echo '<strong>', $caller['file'], ' (line ', $caller['line'], ')</strong>';
    echo '<pre style="background:#fafafa;padding:10px;font-size:13px">';
    print_r($var);
    echo '</pre>';
}

if( function_exists('acf_add_options_page') ):
    acf_add_options_page();
endif;

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

remove_filter ('acf_the_content', 'wpautop');
add_filter ('acf_the_content', 'nl2br');

remove_post_type_support('page', 'editor');
