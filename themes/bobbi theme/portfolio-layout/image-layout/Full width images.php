<section class="layout-img-full-width">
  <div class="container-img">
    <img src="<?php echo get_template_directory_uri(); ?>/img/demoimage1.jpg" alt="">
    <p class='img-number-bottom-corner bottom-corner-left bottom-corner-right'>I.</p>
    <div class="img-text-bottom-corner bottom-corner-left bottom-corner-right">
      <ul>
        <li>(top) flugel horns sound with viewer participation</li>
        <li>(bottom) sound contained using a piece of rubber and a balloon</li>
      </ul>
    </div>

  <div class="container-img">
    <img src="<?php echo get_template_directory_uri(); ?>/img/demoimage2.jpg" alt="">
    <p class='img-number-bottom-corner bottom-corner-left bottom-corner-right'>I.</p>
    <div class="img-text-bottom-corner bottom-corner-left bottom-corner-right">
      <ul>
        <li>(top) flugel horns sound with viewer participation</li>
        <li>(bottom) sound contained using a piece of rubber and a balloon</li>
      </ul>
    </div>
  </div>
</section>
