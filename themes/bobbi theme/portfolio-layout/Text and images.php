
<aside>
  <div class="aside-inner">
    <p>Jacquelyn Bobbi Bortolussi Portfolio 2011–16</p>
    <p>02</p>
  </div>
</aside>
<main class='container-portfolio-post'>
  <article>
    <div class="container-project-title">
      <div class="left">i.</div>
      <div class="right">
        <h1>A stupefied condition, 2014</h1>
        <p>pine wood, flugelhorns, car tire tube, rubber balloons</p>
        <p>38 × 38 × 38 inches</p>
      </div>
    </div>

    <div class="quote">
      <blockquote>
        <p>
          The work doesn’t tell us what to think, nor what to think about: it creates an arena in which thought may take place. It locates us in a sensory field that dislocates other sensory fields and opens us up to an awareness of our senses. It transforms passive unconscious sensing into consciousness, and creates the conditions ”for questioning consciousness itself.
        </p>
        <footer>
          <cite>
            <a href="#">
              — daniel laskarin, curator<br>
            </a>
            <p>excerpt from lull exhibition catalogue featuring a stupified condition</p>
          </cite>
        </footer>
      </blockquote>
    </div>
    <!-- use column-count: css attribute for this section -->
    <p>
      In an era where information is most often processed on two- dimensional screens, this project explores the kind of reactional terrain our preferred technologies are unable to grasp. In order to engage both the mind and the body, I created an environment a viewer can walk through, stand on, and listen to. Using the weight of their own figure, the user can tilt the wooden platform to sound one of the flugelhorns on each of the four sides. They act as spectator, as they activate the piece, and spectacle, as they draw a crowd. Their next move is not only determined by their own reaction but also the surrounding audience. This positions the subject in a way that both engages and removes them from their own involvement. By providing multiple perspectives, this work prompts the viewer to realize that their experience is unique and that each level of involvement is actively developing the installation.
    </p>

    <?php get_template_part('portfolio-layout/image-layout/Full width images'); ?>

  </article>
</main>
